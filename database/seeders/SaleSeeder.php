<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 5; $i++){

            DB::table('sales')->insert([
                'transactions' => $faker->name(30),
                'income' => $faker->numberBetween($min = 1000000, $max = 9999999),
                'created_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Jakarta'),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Jakarta'),
                'order_id' => $faker->numberBetween(1,9),
                
            ]);
            

        }
    }
}
