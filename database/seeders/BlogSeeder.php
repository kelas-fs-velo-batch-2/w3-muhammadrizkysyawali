<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 5; $i++){

            DB::table('blogs')->insert([
                'title' => $faker->text(30),
                'slug' => $faker->slug(30) ,
                'descriptions' => $faker->text(200),
                'created_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Jakarta'),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = 'Asia/Jakarta'),
                'user_id' => $faker->numberBetween(2,6),
                
            ]);

        }
    }
}
